package model;

public class VariableValue {
	
	private Variable variable; 
	private int value; 
	
	public VariableValue(Variable variable, int value) {
		this.variable = variable; 
		this.value = value; 
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "[variable=" + variable + ", value=" + value + "]";
	}
	
	
	
	

}
