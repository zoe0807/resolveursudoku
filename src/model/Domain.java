package model;

import java.util.ArrayList;

public class Domain implements Comparable<Domain>{
	
	private Variable variable; 
	private ArrayList<Integer> domain; 
	
	public Domain(Variable variable, ArrayList<Integer> domain) {
		this.variable = variable; 
		this.domain = domain ;
	}

	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

	public ArrayList<Integer> getDomain() {
		return domain;
	}

	public void setDomain(ArrayList<Integer> domain) {
		this.domain = domain;
	}
	
	public void deleteDomain(Integer value) {
		this.domain.remove(value);
	}

	@Override
	public String toString() {
		return "Domain [variable=" + variable + ", domain=" + domain + "]";
	}

	@Override
	public int compareTo(Domain o) {
		if (this.domain.size() > o.domain.size()) 
			return 1; 
		else if (this.domain.size() == o.domain.size())
			return 0;
		else 
			return -1; 
	}
	
}
