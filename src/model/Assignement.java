package model;

import java.util.ArrayList;

public class Assignement {
	
	private ArrayList<VariableValue> assignement; 
	
	public Assignement() {
		this.assignement = new ArrayList<VariableValue>();
	}
	public Assignement(ArrayList<VariableValue> variablesValues) {
		this.assignement = variablesValues;
	}

	public ArrayList<VariableValue> getAssignement() {
		return assignement;
	}

	public void setAssignement(ArrayList<VariableValue> assignement) {
		this.assignement = assignement;
	}	
	
	
	@Override
	public String toString() {
		String string = ""; 
		string += "Liste des assignements : \n";
		for (VariableValue v : this.assignement) {
			string += "[" + v.toString() + "],";
		}
		return string;
	}

	public void displayAssignements() {
		System.out.println("Liste des assignements faits par Backtracking ("+this.assignement.size()+")");
		for (VariableValue v : this.assignement) {
			System.out.println("[" + v.getVariable().getX() + ";" + v.getVariable().getY() + "] = " + v.getValue());
		}
	}
	
	/**
	 * Fonction qui permet de savoir si un assignement est complet
	 * @param csp
	 * @return
	 */
	public boolean isComplete(CSP csp) {
		for (int i=0; i<9; i++) {
			for (int j=0; j<9; j++) {
				if (!isAssigned(new Variable(i,j)) && csp.domain(new Variable(i,j)).size() != 1)
					return false;
			}
		}
		return true; 
	}
	
	
	/**
	 * Fonction qui permet de savoir si une variable a ete assignee ou non 
	 * @param coords
	 * @return
	 */
	public boolean isAssigned(Variable coords) {
		for (VariableValue v : assignement) {
			if (v.getVariable().equals(coords))
				return true; 
		}
		return false;
	}
	
	/**
	 * Fonction qui permet de savoir si l'assignement est consistant
	 * @param variableValue
	 * @param csp
	 * @return
	 */
	public boolean isConsistent(VariableValue variableValue, CSP csp) {
		
		Assignement nAssignement = new Assignement();
		for (VariableValue var : this.assignement) {
			nAssignement.addAssignement(var);
		}
		nAssignement.addAssignement(variableValue);
		//On peut alors travailler sur une liste qui correspond aux assignements qui avaient deja ete faits
		//ainsi que celui que nous voulons ajouter
		int v1, v2; 
		
		//On verifie pour chaque contrainte si des valeurs ont ete assignees
		//(ou sont assignes de base dans l'algorithme de depart)
		for (ContrainteBinaire contrainte : csp.getContraintes()) {
			
			v1 = -1; v2 = -1; 
		
			for (VariableValue varVal : nAssignement.getAssignement()) {
				if (contrainte.getVariable1().equals(varVal.getVariable())) {
					v1 = varVal.getValue();
				}
				if (contrainte.getVariable2().equals(varVal.getVariable())) {
					v2 = varVal.getValue();
				}
			}
			for (Domain domain : csp.getDomains()) {
				if (domain.getDomain().size() == 1) {
					if (domain.getVariable().equals(contrainte.getVariable1()))
						v1 = domain.getDomain().get(0); 
					else if (domain.getVariable().equals(contrainte.getVariable2()))
						v2 = domain.getDomain().get(0);
				}
			}
			//On verifie si les variables ont �t� assignees et si elles sont egales
			//Si elles sont egales, cela signifie qu'une contrainte n'est pas respectee
			if (v1 != -1 && v1 == v2)
				return false; 
		}
		return true;
		
	}
	
	public void addAssignement(VariableValue variableValue) {
		this.assignement.add(variableValue);
	}
	
	public void removeAssignement(VariableValue variableValue) {
		this.assignement.remove(variableValue);
	}
	
	

}
