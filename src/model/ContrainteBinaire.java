package model;

public class ContrainteBinaire {

	private Variable variable1; 
	private Variable variable2;
	
	public ContrainteBinaire(Variable variable1, Variable variable2) {
		this.variable1 = variable1; 
		this.variable2 = variable2; 
	}

	public Variable getVariable1() {
		return variable1;
	}

	public void setVariable1(Variable variable1) {
		this.variable1 = variable1;
	}

	public Variable getVariable2() {
		return variable2;
	}

	public void setVariable2(Variable variable2) {
		this.variable2 = variable2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((variable1 == null) ? 0 : variable1.hashCode());
		result = prime * result + ((variable2 == null) ? 0 : variable2.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if (object == null) {
            return false;
        }
		
		final ContrainteBinaire contrainte = (ContrainteBinaire) object;
		if(this.getVariable1().equals(contrainte.getVariable1()) && this.getVariable2().equals(contrainte.getVariable2()))
			return true;
		return false;
	}
	
	@Override
	public String toString() {
		return "[variable1=" + variable1 + ", variable2=" + variable2 + "]";
	}
		
	

}
