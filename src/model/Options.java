package model;

/**
 * 
 * Classe Options pour choisir les heuristiques � utiliser avec l'algorithme de Backtracking
 *
 */
public class Options {
	private boolean MRV;
	private boolean DH;
	private boolean LCV;
	
	public Options(boolean MRV, boolean DH, boolean LCV) {
		this.MRV = MRV;
		this.DH = DH;
		this.LCV = LCV;
	}

	public boolean getMRV() {
		return MRV;
	}

	public void setMRV(boolean mRV) {
		MRV = mRV;
	}

	public boolean getDH() {
		return DH;
	}

	public void setDH(boolean dH) {
		DH = dH;
	}

	public boolean getLCV() {
		return LCV;
	}

	public void setLCV(boolean lCV) {
		LCV = lCV;
	}
}
