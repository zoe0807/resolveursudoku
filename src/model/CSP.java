package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CSP {
	
	private ArrayList<Variable> variables; 
	private ArrayList<Domain> domains; 
	private ArrayList<ContrainteBinaire> contraintes; 
	
	public CSP(Sudoku sudoku) {
		this.variables = new ArrayList<Variable>(); 
		for (int i=0; i<9; i++) {
			for (int j=0; j<9; j++)
				variables.add(new Variable(i,j)); 
		}
		
		this.domains = new ArrayList<Domain>(); 
		this.contraintes = new ArrayList<ContrainteBinaire>();
		 
		for (Variable var : this.variables) {
			ArrayList<Integer> domain = new ArrayList<Integer>(); 
			if (sudoku.getGrille()[var.getX()][var.getY()] != 0)
				domain.add(sudoku.getGrille()[var.getX()][var.getY()]);
			else {
				for (int i=1; i<=9; i++)
					domain.add(i);
			}
			this.domains.add(new Domain(var, domain));
			addLineConstraint(var, sudoku); 
			addColumnConstraint(var, sudoku); 
			addSquareConstraint(var, sudoku);
			
		}	
	}
	
	public CSP (CSP csp) {
		this.variables = csp.getVariables(); 
		this.contraintes = csp.getContraintes(); 
		this.domains = csp.getDomains(); 
	}
	
	public ArrayList<Variable> getVariables() {
		return variables;
	}

	public void setVariables(ArrayList<Variable> variables) {
		this.variables = variables;
	}

	public ArrayList<Domain> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<Domain> domains) {
		this.domains = domains;
	}

	public ArrayList<ContrainteBinaire> getContraintes() {
		return contraintes;
	}

	public void setContraintes(ArrayList<ContrainteBinaire> contraintes) {
		this.contraintes = contraintes;
	}


	/**
	 * Fonction qui permet d'ajouter les contraintes sur la ligne de la variable
	 * @param variable
	 * @param sudoku
	 */
	public void addLineConstraint(Variable variable, Sudoku sudoku){
		for (int j=variable.getY()+1; j<9; j++) {
			this.contraintes.add(new ContrainteBinaire(variable, new Variable(variable.getX(),j)));
			this.contraintes.add(new ContrainteBinaire(new Variable(variable.getX(),j), variable)); // contrainte inverse
		} 
	}
	/**
	 * Fonction qui permet d'ajouter les contraintes sur la colonne de la variable
	 * @param variable
	 * @param sudoku
	 */
	public void addColumnConstraint(Variable variable, Sudoku sudoku){

		for (int i=variable.getX()+1; i<9; i++) {
			this.contraintes.add(new ContrainteBinaire(variable, new Variable(i,variable.getY())));
			this.contraintes.add(new ContrainteBinaire(new Variable(i,variable.getY()), variable)); // contrainte inverse
		}
	}
	/**
	 * Fonction qui permet d'ajouter les contraintes dans le carre de la variable
	 * @param variable
	 * @param sudoku
	 */
	public void addSquareConstraint(Variable variable, Sudoku sudoku) {
		
		int[] square = chooseSquare(variable);
		
		for (int i=square[0]; i< square[0]+3; i++) {
			for (int j=square[1]; j<square[1]+3; j++) {
				if (variable.getX() != i && variable.getY() != j) { //On verifie qu'on n'est pas sur la variable visee
					ContrainteBinaire contrainte = new ContrainteBinaire(variable, new Variable(i,j));
					ContrainteBinaire contrainteInverse = new ContrainteBinaire(new Variable(i,j), variable);
					if(!this.contraintes.contains(contrainte)) {
						this.contraintes.add(contrainte);
						this.contraintes.add(contrainteInverse);
					}
				}
			}
		}
	}
	
	/**
	 * Fonction qui permet de choisir le carre sur lequel se trouve la variable
	 * @param variable
	 * @return
	 */
	public static int[] chooseSquare(Variable variable) {
		int[] square = new int[2];
		if (variable.getX() < 3)
			square[0] = 0; 
		else if (variable.getX() < 6)
			square[0] = 3; 
		else
			square[0] = 6;
		
		if (variable.getY() < 3)
			square[1] = 0; 
		else if (variable.getY() < 6)
			square[1] = 3; 
		else
			square[1] = 6;
			
		return square; 
	}
	
	/**
	 * Fonction qui permet d'avoir les voisins d'une variable
	 * @param var
	 * @return
	 */
	public ArrayList<Variable> getNeighbors(Variable var){
		
		ArrayList<Variable> neighbors = new ArrayList<Variable>(); 
		for (ContrainteBinaire contrainte : this.contraintes) {
			if (contrainte.getVariable1().equals(var))
				neighbors.add(contrainte.getVariable2()); 
			else if (contrainte.getVariable2().equals(var)) {
				neighbors.add(contrainte.getVariable1());
			}
		}
			
		return neighbors; 
	}
	
	/**
	 * Fonction qui permet d'avoir le domaine d'une variable 
	 * @param var
	 * @return
	 */
	public ArrayList<Integer> domain(Variable var){
		
		for (Domain domain : this.domains) {
			if (domain.getVariable().equals(var))
				return domain.getDomain();
		}
		return new ArrayList<Integer>(); 
	}
	
	/**
	 * Fonction qui permet de voir si une valeur satisfait une contrainte
	 * @param arc
	 * @param x
	 * @return
	 */
	public boolean valueSatisfyConstraint(ContrainteBinaire arc, int x) {
		for (int y : domain(arc.getVariable2())) {
			if (x != y)
				return true;
		}
		return false; 
	}
	
	/**
	 * Fonction qui permet de supprimer un domaine
	 * @param var
	 * @param value
	 */
	public void deleteDomain(Variable var, int value) {
		for (Domain domain : this.domains) {
			if (domain.getVariable().equals(var))
				domain.deleteDomain(value);
		}
	}

	/**
	 * Fonction qui permet d'afficher le nombre total des domaines additiones de toutes les variables
	 * @return
	 */
	public int sizeDomain() {
		int cpt=0;
		for (Domain domain : this.domains) 
			cpt += domain.getDomain().size();
		return cpt; 
	}
	
	/**
	 * Fonction qui permet d'avoir les variables qui ne sont pas encore assignees
	 * @param assignement
	 * @return
	 */
	public ArrayList<Variable> getUnassignedVariables(Assignement assignement) {
		ArrayList<Variable> unassignedVariables = new ArrayList<Variable>(); 
		for (Variable variable : this.variables) {
			boolean assigned = false;
			for (VariableValue assignedVariable : assignement.getAssignement()) {
				if (assignedVariable.getVariable().equals(variable))
					assigned = true;
			}
			for (Domain domain : this.domains) {
				if (domain.getVariable() == variable && domain.getDomain().size() == 1) 
					assigned = true; 
			}
			if (assigned == false)
				unassignedVariables.add(variable);
		}
		return unassignedVariables; 
	}
	
	/**
	 * Fonction qui echange le domaine de la variable par l'assignement (choisi par Backtracking)
	 * @param assignements
	 */
	public void applyAssignements(Assignement assignements) {
		for(VariableValue assignement : assignements.getAssignement()) {
			domain(assignement.getVariable()).clear();
			domain(assignement.getVariable()).add(assignement.getValue());
		}
	}
}
