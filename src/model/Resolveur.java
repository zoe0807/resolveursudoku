package model;

import java.util.ArrayList;
import java.util.Collections;

public class Resolveur {
	
	public static int iterations; 

	/**
	 * 
	 *  AC3
	 *
	 */
	public static CSP ac3(CSP csp) {
		CSP newCsp = new CSP(csp); 
		ArrayList<ContrainteBinaire> queue = new ArrayList<ContrainteBinaire>(newCsp.getContraintes());
		while (!queue.isEmpty()) {
			ContrainteBinaire arc = queue.remove(0);
			if (removeInconsistentValues(arc, newCsp)) {
				for (Variable var : newCsp.getNeighbors(arc.getVariable1())) {
					queue.add(new ContrainteBinaire(var, arc.getVariable1()));
				}
			}
		}
		return newCsp; 
	}
	
	public static boolean removeInconsistentValues(ContrainteBinaire arc, CSP csp) {
		for (int x : csp.domain(arc.getVariable1())) {
			if (!csp.valueSatisfyConstraint(arc,x)){
				csp.deleteDomain(arc.getVariable1(), x);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Fonction qui verifie si le sudoku a �t� resolu
	 * @param csp
	 * @return
	 */
	public static boolean isResolved(CSP csp) {
		if(csp.getVariables().size() != 81)
			return false;
		for(Variable variable : csp.getVariables()) {
			if(csp.domain(variable).size() != 1)
				return false;
		}
		return true;
	}
	
	/**
	 * 
	 *  Backtracking
	 *
	 */
	public static Assignement backtrackingSearch(CSP csp, Options options) {
		iterations = 0; 
		return recursiveBacktracking(new Assignement(), csp, options);
	}
	
	public static Assignement recursiveBacktracking(Assignement assignement, CSP csp, Options options) {
		iterations++;
		ArrayList<Integer> listDomains = null;
		Assignement result; 
		if (assignement.isComplete(csp)) {
			//System.out.println("Iterations : " + iterations);
			return assignement;
		}
		Variable var = null;
		if(options.getMRV() && options.getDH()) // Utilisation de MRV et DH (pour trancher en cas d'egalite)
			var = chooseVariableWithHeuristics(csp.getUnassignedVariables(assignement),csp,assignement);
		else if(options.getMRV()) // Utilisation uniquement de MRV
			var = minimumRemainingValue(csp.getUnassignedVariables(assignement),csp).get(0);
		else if(!options.getMRV() && !options.getDH()) // Pas d'heuristique pour le choix de la variable
			var = csp.getUnassignedVariables(assignement).get(0);
		
		if(options.getLCV()) // Utilisation de LCV
			listDomains = leastConstrainingValue(var, csp, assignement);
		else
			listDomains = csp.domain(var);

		for (int value : listDomains) {			
			VariableValue newAssignement = new VariableValue(var,value);
			
			if (assignement.isConsistent(newAssignement, csp)) {
				
				assignement.addAssignement(newAssignement);
				result = recursiveBacktracking(assignement, csp, options);

				if (result != null)			
					return result;
				assignement.removeAssignement(newAssignement);
			}
		}
		return null;	
	}
	
	
	public static ArrayList<Variable> minimumRemainingValue(ArrayList<Variable> variables, CSP csp) {
		ArrayList<Variable> list = new ArrayList<Variable>();
		ArrayList<Variable> vars = new ArrayList<>(variables); // pour copier les valeurs et non les references
		
		for (Variable var : vars) {
			list.add(var);
		}
		
		list.add(vars.get(0)); 
		int minDomain = csp.domain(vars.get(0)).size();
		
		for (Variable variable : vars) {
			if (csp.domain(variable).size() < minDomain) {
				list.clear();
				list.add(variable);
				minDomain = csp.domain(variable).size();
			}
			else if (csp.domain(variable).size() == minDomain)
				list.add(variable); 
		}
		return list; 	
	}

	/**
	 * Fonction qui permet de trancher en cas d'egalite dans minimumRemainingValue
	 * @param variables
	 * @param csp
	 * @param assignement
	 * @return
	 */
	public static Variable degreeHeuristic(ArrayList<Variable> variables, CSP csp, Assignement assignement) {
		ArrayList<Variable> vars = new ArrayList<Variable>(variables);
		int maxContraintes = 0; 
		Variable variable = null; 
		for (Variable var : vars) {
			int contraintes = 0; 
			for (Variable v : csp.getNeighbors(var)) {
				boolean assigned = false; 
				if (!assignement.isAssigned(v) && csp.domain(v).size() != 1)
					contraintes++; 				
			}
			
			if (contraintes > maxContraintes) {
				maxContraintes = contraintes; 
				variable = var; 	
			}
		}
		return variable; 
	}
	
	/**
	 * Fonction qui permet d'utiliser les heuristiques MRV + DH
	 * @param variables
	 * @param csp
	 * @param assignement
	 * @return
	 */
	public static Variable chooseVariableWithHeuristics(ArrayList<Variable> variables, CSP csp, Assignement assignement) {
		return degreeHeuristic(minimumRemainingValue(variables, csp), csp, assignement);
	}
	
	public static ArrayList<Integer> leastConstrainingValue(Variable variable, CSP csp, Assignement assignement) {
		
		ArrayList<ValueConstraint> list = new ArrayList<ValueConstraint>();
		ArrayList<Integer> sortedList = new ArrayList<Integer>();
		
		for (int value : csp.domain(variable)) {
			int constraint = 0; 
			for (Variable neighbor : csp.getNeighbors(variable)) {
				if (!assignement.isAssigned(neighbor) && csp.domain(neighbor).size() != 1) {
					if (csp.domain(neighbor).contains(value)) {
						constraint ++; 
					}
				}
			}
			list.add(new ValueConstraint(value,constraint));
		}
		Collections.sort(list);
		for (ValueConstraint valueConstraint : list) {
			sortedList.add(valueConstraint.getValue());
		}
		return sortedList; 
		
		
	}
}
