package model;

public class ValueConstraint implements Comparable<ValueConstraint> {
	
	//Cette classe est utilisee dans Least Constraining Value
	//Elle permet de faire une liste qui nous donne le nombre de contraintes pour chaque valeur
	//On peut ainsi la trier gr�ce au compareTo
	
	private int value; 
	private int constraint; 
	
	public ValueConstraint(int value, int constraint) {
		this.value = value; 
		this.constraint = constraint; 
	}
	
	public int getValue() {
		return this.value; 
	}
	
	public void setValue(int value) {
		this.value= value; 
	}
	
	public int getConstraint() {
		return this.constraint; 
	}
	
	public void setConstraint(int constraint) {
		this.constraint = constraint; 
	}

	@Override
	public int compareTo(ValueConstraint arg0) {
		if (this.constraint > arg0.getConstraint())
			return 1; 
		else if (this.constraint == arg0.getConstraint())
			return 0;
		return -1;
	}
	
	
	

}
