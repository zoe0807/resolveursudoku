package model;

public class Sudoku {
	
	private int[][] grille; 
	
	public Sudoku(int[][] grille) {
		this.grille = grille; 
	}
	
	public Sudoku(String stringSudoku) {
		int[][] grille = new int [9][9];
		int cpt = 0;
		for(int i = 0; i < 9; i++) {
			for(int j = 0; j < 9; j++) {
				grille[i][j] = Integer.parseInt(String.valueOf(stringSudoku.charAt(cpt)));
				cpt++;
			}
		}
		this.grille = grille;
	}

	public int[][] getGrille() {
		return grille;
	}

	public void setGrille(int[][] grille) {
		this.grille = grille;
	}

}
