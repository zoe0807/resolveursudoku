package view;


import java.util.ArrayList;

import model.CSP;
import model.ContrainteBinaire;
import model.Sudoku;
import model.Variable;

public class Display {
	
	public static void displayGrille(Sudoku sudoku) {
		
		for (int i=0; i<9; i++) {
			if (i==3 || i==6) {
				for (int j=0; j<10; j++) {
					System.out.print("--");
				}
				System.out.println();
			}
			for (int j=0; j<9; j++) {
				if ( j==3 || j==6)
					System.out.print("|"); 
				if (sudoku.getGrille()[i][j] == 0)
					System.out.print(". ");
				else
					System.out.print(sudoku.getGrille()[i][j] + " ");
			}
			System.out.println();
			
		}
	}
	
	public static void displaySudokuFromCSP(CSP csp)
	{
		for (int i=0; i<9; i++) {
			if (i==3 || i==6) {
				for (int j=0; j<10; j++) {
					System.out.print("--");
				}
				System.out.println();
			}
			for (int j=0; j<9; j++) {
				if ( j==3 || j==6)
					System.out.print("|"); 
				if (csp.domain(new Variable(i,j)).size() == 0)
					System.out.print(". ");
				else if(csp.domain(new Variable(i,j)).size() > 1)
					System.out.print(". ");
				else
					System.out.print(csp.domain(new Variable(i,j)).get(0) + " ");
			}
			System.out.println();
			
		}
	}


}
