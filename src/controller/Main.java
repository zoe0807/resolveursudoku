package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.Assignement;
import model.CSP;
import model.Options;
import model.Resolveur;
import model.Sudoku;
import view.Display;

public class Main {
	
	
	public static void main(String[] args) {
		
		File file = new File("sudoku.txt"); 
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String st;
		String stringSudoku = "";
		try {
			while ((st = br.readLine()) != null) 
				stringSudoku += st;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int[][] sudokuSolvableByAC3 = {{0,0,3,0,2,0,6,0,0},
				          			   {9,0,0,3,0,5,0,0,1},
				          			   {0,0,1,8,0,6,4,0,0},
				          			   {0,0,8,1,0,2,9,0,0},
				          			   {7,0,0,0,0,0,0,0,8},
				          			   {0,0,6,7,0,8,2,0,0},
				          			   {0,0,2,6,0,9,5,0,0},
				          			   {8,0,0,2,0,3,0,0,9},
				          			   {0,0,5,0,1,0,3,0,0}};
		
		int[][] grille = {{0,0,0,1,0,0,7,0,2},
				          {0,3,0,9,5,0,0,0,0},
				          {0,0,1,0,0,2,0,0,3},
				          {5,9,0,0,0,0,3,0,1},
				          {0,2,0,0,0,0,0,7,0},
				          {7,0,3,0,0,0,0,9,8},
				          {8,0,0,2,0,0,1,0,0},
				          {0,0,0,0,8,5,0,6,0},
				          {6,0,5,0,0,9,0,0,0}};
		
		int[][] grille2 = {{0,0,0,2,6,0,7,0,1},
				           {6,8,0,0,7,0,0,9,0},
				           {1,9,0,0,0,4,5,0,0},
				           {8,2,0,1,0,0,0,4,0},
				           {0,0,4,6,0,2,9,0,0},
				           {0,5,0,0,0,3,0,2,8},
				           {0,0,9,3,0,0,0,7,4},
				           {0,4,0,0,5,0,0,3,6},
				           {7,0,3,0,1,8,0,0,0}};
		
		
		int[][] intermediaire = {{0,2,0,6,0,8,0,0,0},
					             {5,8,0,0,0,9,7,0,0},
					             {0,0,0,0,4,0,0,0,0},
					             {3,7,0,0,0,0,5,0,0},
			   		             {6,0,0,0,0,0,0,0,4},
					             {0,0,8,0,0,0,0,1,3},
					             {0,0,0,0,2,0,0,0,0},
					             {0,0,9,8,0,0,0,3,6},
					             {0,0,0,3,0,6,0,9,0}};
		

		//Sudoku sudoku = new Sudoku(sudokuSolvableByAC3); // Cr�ation du Sudoku
		Sudoku sudoku = new Sudoku(stringSudoku);
		
		Display.displayGrille(sudoku);
		CSP csp = new CSP(sudoku); // Transformation du Sudoku en CSP
		
		int domainSize = csp.sizeDomain(); // Calcul de la taille du domaine initial

		CSP cspWithReducedDomains = Resolveur.ac3(csp); // Appel de AC3

		if(Resolveur.isResolved(cspWithReducedDomains)) { // AC3 a resolu le Sudoku
			System.out.println("\nAC3 a r�solu le sudoku ! \n");
			Display.displaySudokuFromCSP(cspWithReducedDomains);
		}
		else { // Le sudoku n'est pas encore resolu, on lance l'algorithme de Backtracking
			System.out.println("\nAC3 a r�duit le domaine du CSP de " + (domainSize - cspWithReducedDomains.sizeDomain()) + " !");
			System.out.println("D�marrage du backtracking");
			Options options = new Options(false, false, false); // choix des heuristiques (en ordre MRV, DH, LCV, mettre a true pour les activer)
			Assignement listAssignements = Resolveur.backtrackingSearch(cspWithReducedDomains, options);
			System.out.println("\nBacktracking termin�, nombre d'it�rations : " + Resolveur.iterations);

			cspWithReducedDomains.applyAssignements(listAssignements); // On ajoute les assignements trouv�s par Backtracking au CSP
			Display.displaySudokuFromCSP(cspWithReducedDomains); // Affichage du Sudoku r�solu		
		}
	}

}
